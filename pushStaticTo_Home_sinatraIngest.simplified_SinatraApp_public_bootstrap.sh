#!/bin/sh

rm -r ./vue-spa.output/*
rm -r ~/sinatraIngest.simplified/SinatraApp/public/ingestFrontend/*
docker-compose run vue-spa npm run build
# sed -i 's/\/static\//\//g' ~/sinatraIngest.simplified/SinatraApp/public/bootstrap/build.js
cp -r ./vue-spa.output/* ~/sinatraIngest.simplified/SinatraApp/public/ingestFrontend/
