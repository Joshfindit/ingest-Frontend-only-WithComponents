// webpack.config.js
module.exports = {
    devServer: {
      inline:true,
      host: process.env.EXTERNAL_HOST,
      port: process.env.EXTERNAL_PORT
    },
    plugins: [
        new webpack.ProvidePlugin({
           $: 'jquery',
           jQuery: 'jquery'
       })
    ]
};
