import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Timer from '@/components/Timer'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    { path: '/user/:id', component: Hello },
    {
      path: '/uuid/:uuid',
      name: 'byuuid',
      component: Hello
    },
    {
      path: '/timer',
      name: 'Timer',
      component: Timer
    }

  ]
})
