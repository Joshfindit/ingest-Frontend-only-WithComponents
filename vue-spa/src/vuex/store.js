import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import * as actions from './actions'

Vue.use(Vuex)

const state = {
  noteList: [],
  artefacts: [],
  activeArtefact: {
    artefacts: [],
    name: '',
    description: '',
    content: '',
    id: '',
    uuid: ''
  },
  isArtefactLoading: false,
  isPostNewArtefactLoading: false,
  isArtefactListLoading: false,
  isShowArtefactEdit: false,
  isShowNewArtefactForm: false,
  websession: '6fe5bd9c-d355-42a1-b9a6-b1be20568e64'
}

// helper methods (internal)

function checkArtefactsForID (artefactuuid) {
  var potentialIndex = state.artefacts.findIndex(artefact => artefact.uuid === artefactuuid)
  if (potentialIndex >= 0) { // findIndex returns -1 if not found
    console.log('returning index for state.artefacts.findIndex(artefact => artefact.uuid === artefactuuid)')
    return potentialIndex
  } else {
    return false
  }
}

// end helper methods (internal)

const mutations = {

  SET_WEBSESSION (state, uuid) {
    state.websession = uuid
  },

  SET_IS_ARTEFACT_LOADING (state, bool) {
    state.isArtefactLoading = bool
  },

  SET_IS_ARTEFACT_LIST_LOADING (state, bool) {
    state.isArtefactListLoading = bool
  },

  GET_NOTE_LIST (state, inboxbool) {
    // this.loading = true;
    // this.jsonIndex = [];
    state.isArtefactListLoading = true
    var url = null

    if (inboxbool) {
      url = encodeURI('/inbox_artefacts/index.json')
    } else {
      url = encodeURI('/artefacts/index.json')
    }

    axios.get(url, {headers: {'Authorization': 'Token token=' + state.websession}}).then(
      function (request) {
        // success callback

        var simpleArray = []

        var parsedJSON = request.data

        for (var propertyIndex in parsedJSON) {
          if (parsedJSON[propertyIndex].artefact) {
            var setText
            if (!parsedJSON[propertyIndex].artefact.content) {
              setText = parsedJSON[propertyIndex].artefact.description
            } else {
              setText = parsedJSON[propertyIndex].artefact.content
            }

            var thisArtefact = parsedJSON[propertyIndex].artefact
            thisArtefact.content = setText
            thisArtefact.favorite = false

            // simpleArray.push({ favorite: false, name: parsedJSON[propertyIndex].artefact.name, text: setText, uuid: parsedJSON[propertyIndex].artefact.id});
            simpleArray.push(thisArtefact)
          }
        }

        // var simpleArray = [];
        // for (var i = 1; i <= 7; i++) { // > # Fixing nano syntax highlighting again
        //   simpleArray.push(i);
        //   console.log(parsedJSON[i])
        // };
        // this.setJsonIndex (simpleArray);

        // Sort by name
        simpleArray.sort(function (a, b) {
          var textA
          var textB

          if (a.name) {
            textA = a.name.toUpperCase()
          } else {
            textA = ''
          }
          if (b.name) {
            textB = b.name.toUpperCase()
          } else {
            textB = ''
          }
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0
        })

        state.artefacts = simpleArray
        // this.loading = false;
        state.isArtefactListLoading = false
      // }.bind(this),
      },
      function (response) {
        // error callback
        // this.loading = false;
        state.isArtefactListLoading = false
      }
    )

    // state.isArtefactListLoading = false
    // url = encodeURI("https://jsonplaceholder.typicode.com/photos/" + "1")
    // $.getJSON(url, function (parsedJSON) {
    //   v.jsonIndex = parsedJSON;
    // })
  },

  GET_SINGLE_NOTE (state) {
    // this.loading = true;
    // this.artefact = getBlankArtefactData();
    var uuid = '8f466172-3c6e-431f-b430-05a5e849ac5f'
    var url = encodeURI('/artefacts/' + uuid + '.json')

    axios.get(url, {headers: {'Authorization': 'Token token=' + state.websession}}).then(
      function (request) {
        // success callback
        var returnedJSON = request.data.artefact
        var thisNote = returnedJSON
        // this.response = request.data;
        // console.log(returnedJSON);
        // thisNote.uuid = returnedJSON.id;
        // thisNote.favorite = true;
        // thisNote.name = returnedJSON.name;
        // thisNote.text = returnedJSON.content;  // Change instances of text throughout
        // thisNote.description = returnedJSON.description;
        thisNote.artefacts = []
        for (var thisArtefact in returnedJSON.artefacts) {
          // console.log(returnedJSON.artefacts[thisArtefact]);
          thisNote.artefacts.push(returnedJSON.artefacts[thisArtefact].id)
        }
        // this.artefact.identities_with_permission_display = [];
        // for (thisIdentity in returnedJSON.identities_with_permission){
        //   //console.log(returnedJSON.identities_with_permission[thisIdentity]);
        //   this.artefact.identities_with_permission_display.push(returnedJSON.identities_with_permission[thisIdentity].username);
        // };
        // this.artefact.connectedArtefacts = returnedJSON.artefacts;
        // // console.log(returnedJSON.artefacts);
        // // console.log(this.artefact.connectedArtefacts);
        // //this.thumbnail = parsedJSON.thumbnailUrl;
        // returnedJSONMinusRaw = returnedJSON;
        // this.deleteFromObjectByKeyPart ("Raw", returnedJSONMinusRaw);
        // // delete parsedJSONMinusRaw["originalMetaDataDumpRaw"];
        // // v.json = parsedJSON;
        // // this.json = parsedJSONMinusRaw;
        // this.json = returnedJSONMinusRaw.artefact;
        // this.loading = false;

        state.artefacts.push(thisNote)

      // }.bind(this),
      },
      function (response) {
        // error callback
        // this.json = {error: "Error"};
        // this.loading = false;
      }
    )
  },

  // ADD_NOTE (state) {
  //   const newNote = {
  //     name: 'New Note Name',
  //     content: 'New note',
  //     favorite: false,
  //     uuid: null,
  //     id: null
  //   }
  //   state.artefacts.push(newNote)
  //   state.activeNote = newNote
  // },

  // EDIT_NOTE (state, text) {
  //   state.activeNote.text = text
  // },
  UPDATE_ACTIVE_ARTEFACT_CONNECTED_ARTEFACTS (state, arrayofuuids) {
    state.activeArtefact.artefacts = arrayofuuids
  },

  EDIT_ARTEFACT (state, artefact) {
    state.isArtefactLoading = true
    console.log('PUTing. Data being sent follows.')
    console.log(JSON.stringify(artefact))

    var patchUrl = encodeURI('/artefacts/' + artefact.uuid + '.json')
    axios.put(patchUrl, {artefact: artefact}, {headers: {'Authorization': 'Token token=' + state.websession}}).then((request) => {
      // success callback
      console.log('Successfully PUT. Setting state.activeArtefact')
      state.activeArtefact = request.data.artefact
      state.isArtefactLoading = false
    }, (error) => {
      console.log(error)
      // TODO: Return the error to the user
      state.isArtefactLoading = false
    })
  },

  SET_IS_POST_NEW_ARTEFACT_LOADING (state, bool) {
    state.isPostNewArtefactLoading = bool
  },

  POST_NEW_ARTEFACT (state, artefact) {
    state.isPostNewArtefactLoading = true
    console.log('POSTing. Data being sent follows.')
    console.log(JSON.stringify(artefact))
    var patchUrl = encodeURI('/artefacts')
    axios.post(patchUrl, {artefact: artefact}, {headers: {'Authorization': 'Token token=' + state.websession}}).then((request) => {
      // success callback
      // TODO: Show a success message to the user
      // state.activeArtefact = request.data.artefact // If we're going to do this, we should route to request.data.uuid instead
      state.isPostNewArtefactLoading = false
    }, (error) => {
      console.log(error)
      // TODO: Return the error to the user
      state.isPostNewArtefactLoading = false
    })
  },

  SET_SHOW_ARTEFACT_EDIT (state, bool) {
    state.isShowArtefactEdit = bool
  },

  TOGGLE_SHOW_ARTEFACT_EDIT (state) {
    state.isShowArtefactEdit = !state.isShowArtefactEdit
  },

  SET_SHOW_NEW_ARTEFACT_FORM (state, bool) {
    state.isShowNewArtefactForm = bool
  },

  TOGGLE_SHOW_NEW_ARTEFACT_FORM (state) {
    state.isShowNewArtefactForm = !state.isShowNewArtefactForm
  },

  // DELETE_NOTE (state) {
  //   var index = state.artefacts.indexOf(state.activeNote)
  //   state.artefacts.splice(index, 1)
  //   state.activeNote = state.artefacts.length > 0 ? state.artefacts[0] : {}
  // },

  DELETE_ARTEFACT_FROM_API (state, artefactuuid) {
    state.isArtefactLoading = true
    var url = encodeURI('/artefacts/' + artefactuuid + '.json')
    axios.delete(url, {headers: {'Authorization': 'Token token=' + state.websession}}).then((request) => {
      // success callback
      state.activeArtefact = {}
      state.isArtefactLoading = false
      // TODO: Refresh the note list
    }, (error) => {
      console.log(error)
      // TODO: Return the error to the user
      state.isArtefactLoading = false
    })
  },

  // TOGGLE_FAVORITE (state) {
  //   state.activeNote.favorite = !state.activeNote.favorite
  // },

  SET_ACTIVE_ARTEFACT (state, artefact) {
    state.activeArtefact = artefact
  },

  FLUSH_STORE_AND_SET_ACTIVE_ARTEFACT_BY_UUID (state, artefactuuid) {
    var foundIndex = checkArtefactsForID(artefactuuid)
    if (foundIndex === 0 || foundIndex > 0) {
      console.log('Found Index!' + foundIndex)
      console.log('Flushing Index')
      state.artefacts.splice(foundIndex, 1)

      state.isArtefactLoading = true
      var url = encodeURI('/artefacts/' + artefactuuid + '.json')
      axios.get(url, {headers: {'Authorization': 'Token token=' + state.websession}}).then((request) => {
        // success callback
        state.artefacts.push(request.data.artefact)
        state.activeArtefact = request.data.artefact
        state.isArtefactLoading = false
      }, (error) => {
        console.log(error)
        state.isArtefactLoading = false
      })
    } else {
      state.mutations.GET_AND_SET_ACTIVE_ARTEFACT_FROM_API(state, artefactuuid)
    }
  },

  CHECK_STORE_AND_SET_ACTIVE_ARTEFACT_BY_UUID (state, artefactuuid) {
    var foundIndex = checkArtefactsForID(artefactuuid)
    if (foundIndex === 0 || foundIndex > 0) {
      console.log('Found Index!' + foundIndex)
      if (state.artefacts[foundIndex].content) {
        console.log('Found content as well. Setting activeArtefact')
        state.activeArtefact = state.artefacts[foundIndex]
      } else {
        console.log('Did NOT find content. TODO: Get from API.')
        state.activeArtefact = state.artefacts[foundIndex]
      }
    } else {
      console.log('Getting from the API')
      state.isArtefactLoading = true
      var url = encodeURI('/artefacts/' + artefactuuid + '.json')
      axios.get(url, {headers: {'Authorization': 'Token token=' + state.websession}}).then((request) => {
        // success callback
        state.artefacts.push(request.data.artefact)
        state.activeArtefact = request.data.artefact
        state.isArtefactLoading = false
      }, (error) => {
        console.log(error)
        state.isArtefactLoading = false
      })
    }
  },

  GET_AND_SET_ACTIVE_ARTEFACT_FROM_API (state, artefactuuid) {
    state.isArtefactLoading = true
    var url = encodeURI('/artefacts/' + artefactuuid + '.json')
    axios.get(url, {headers: {'Authorization': 'Token token=' + state.websession}}).then((request) => {
      // success callback
      state.artefacts.push(request.data.artefact)
      state.activeArtefact = request.data.artefact
      state.isArtefactLoading = false
    }, (error) => {
      console.log(error)
      state.isArtefactLoading = false
    })
  }
}

const getters = {
  isArtefactListLoading: state => state.isArtefactListLoading,
  isArtefactLoading: state => state.isArtefactLoading,
  isPostNewArtefactLoading: state => state.isPostNewArtefactLoading,
  isShowArtefactEdit: state => state.isShowArtefactEdit,
  isShowNewArtefactForm: state => state.isShowNewArtefactForm,
  activeArtefact: state => state.activeArtefact,
  websession: state => state.websession,
  artefacts: state => state.artefacts
}

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})
