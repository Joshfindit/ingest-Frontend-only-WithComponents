export const setWebsession = ({ commit }, uuid) => {
  commit('SET_WEBSESSION', uuid)
}

export const isArtefactListLoading = ({ commit }, bool) => {
  commit('SET_IS_ARTEFACT_LIST_LOADING', bool)
}

export const isArtefactLoading = ({ commit }, bool) => {
  commit('SET_IS_ARTEFACT_LOADING', bool)
}

export const setShowArtefactEdit = ({ commit }, bool) => {
  commit('SET_SHOW_ARTEFACT_EDIT', bool)
}

export const toggleShowArtefactEdit = ({ commit }, bool) => {
  commit('TOGGLE_SHOW_ARTEFACT_EDIT', bool)
}

export const setIsPostNewArtefactLoading = ({ commit }, bool) => {
  commit('SET_IS_POST_NEW_ARTEFACT_LOADING', bool)
}

export const setShowNewArtefactForm = ({ commit }, bool) => {
  commit('SET_SHOW_NEW_ARTEFACT_FORM', bool)
}

export const toggleShowNewArtefactForm = ({ commit }, bool) => {
  commit('TOGGLE_SHOW_NEW_ARTEFACT_FORM', bool)
}

export const getNoteList = ({ commit }, inboxbool) => {
  commit('GET_NOTE_LIST', inboxbool)
}

export const getSingleNote = ({ commit }) => {
  commit('GET_SINGLE_NOTE')
}

export const addNote = ({ commit }) => {
  commit('ADD_NOTE')
}

export const updateActiveArtefactConnectedArtefacts = ({ commit }, arrayofuuids) => {
  commit('UPDATE_ACTIVE_ARTEFACT_CONNECTED_ARTEFACTS', arrayofuuids)
}

export const editArtefact = ({ commit }, artefact) => {
  commit('EDIT_ARTEFACT', artefact)
}

export const postArtefact = ({ commit }, artefact) => {
  commit('POST_ARTEFACT', artefact)
}

export const postNewArtefact = ({ commit }, artefact) => {
  commit('POST_NEW_ARTEFACT', artefact)
}

// export const deleteNote = ({ commit }) => {
//   commit('DELETE_NOTE')
// }

export const deleteNote = ({ commit }) => {
  // commit('DELETE_NOTE')
  console.log('Called deleteNote')
}

export const deleteArtefactFromAPI = function ({ commit }, artefactuuid) {
  commit('DELETE_ARTEFACT_FROM_API', artefactuuid)
}

export const getAndSetActiveArtefactFromAPI = ({ commit }, artefactuuid) => {
//  commit('GET_AND_SET_ACTIVE_ARTEFACT_FROM_API', artefactuuid)
  commit('CHECK_STORE_AND_SET_ACTIVE_ARTEFACT_BY_UUID', artefactuuid)
}

export const flushStoreAndSetActiveArtefactByUUID = ({ commit }, artefactuuid) => {
  commit('FLUSH_STORE_AND_SET_ACTIVE_ARTEFACT_BY_UUID', artefactuuid)
}

export const setActiveArtefact = ({ commit }, artefact) => {
  commit('SET_ACTIVE_ARTEFACT', artefact)
}

export const toggleFavorite = ({ commit }) => {
  commit('TOGGLE_FAVORITE')
}
